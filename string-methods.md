# Methods on Strings

### `str.casefold()`
Casefolding is similar to lowercasing but more aggressive because it is intended to remove all case distinctions in a string. For example, the German lowercase letter `'ß'` is equivalent to `"ss"`. Since it is already lowercase, `lower()` would do nothing to `'ß'`; `casefold()` converts it to `"ss"`.

### `str.count(sub[, start[, end]])`
Return the number of non-overlapping occurrences of substring sub in the range `[start, end]`. Optional arguments `start` and `end` are interpreted as in slice notation.

### `str.endswith(suffix[, start[, end]])` and `startswith()`
`True` if the string ends with the specified suffix.

### `str.index(sub[, start[, end]])`
Return the *lowest* index in the string where substring sub is found within the slice `s[start:end]`. *Raise `ValueError` when the substring is not found.*

### `str.isalnum()`, `str.isalpha()`, `str.isdecimal()`, `str.isnumeric()`
### `str.istitle()`

### `str.lstrip([chars])`
Return a copy of the string with leading characters removed. The `chars` argument is a string specifying the set of characters to be removed. *If omitted or None, the chars argument defaults to removing whitespace*. The chars argument is not a prefix; rather, all combinations of its values are stripped:
```python
>>> '   spacious   '.lstrip()
'spacious   '
>>> 'www.example.com'.lstrip('cmowz.')  # Observe
'example.com'
```

### `str.replace(old, new[, count])`
Return a copy of the string with all occurrences of substring `old` replaced by `new`. If the optional argument `count` is given, only the first `count` occurrences are replaced.

### `str.rindex(sub[, start[, end]])`
Return the *highest index* in the string where substring `sub` is found, such that sub is contained within `s[start:end]`. Raises ValueError when the substring sub is not found.

### `str.split(sep=None, maxsplit=-1)`
Return a list of the words in the string, using sep as the delimiter string. If `maxsplit` is given, at most `maxsplit` splits are done (thus, the list will have at most `maxsplit+1` elements).

If `sep` is given, consecutive delimiters are not grouped together and are deemed to delimit empty strings (for example, `'1,,2'.split(',') returns ['1', '', '2']`).

The `sep` argument may consist of multiple characters (for example, `'1<>2<>3'.split('<>')` returns `['1', '2', '3']`). Splitting an empty string with a specified separator returns `['']`.

```python
>>> '1,2,3'.split(',')
['1', '2', '3']
>>> '1,2,3'.split(',', maxsplit=1) # Observe
['1', '2,3']
>>> '1,2,,3,'.split(',')
['1', '2', '', '3', ''] # Observe
```
If `sep` is not specified or is `None`, a different splitting algorithm is applied: runs of consecutive whitespace are regarded as a single separator, and the result will contain no empty strings at the start or end if the string has leading or trailing whitespace. Consequently, splitting an empty string or a string consisting of just whitespace with a `None` separator returns [].

```python
>>> '1 2 3'.split()
['1', '2', '3']
>>> '1 2 3'.split(maxsplit=1)
['1', '2 3']
>>> '   1   2   3   '.split()
['1', '2', '3']
```

### `str.splitlines([keepends])`
Handles many kinds of separators. Observe the difference between this and `split()`
```python
>>> "".splitlines()
[]
>>> "One line\n".splitlines()
['One line']
```

and

```python
>>> ''.split('\n')
['']
>>> 'Two lines\n'.split('\n')
['Two lines', '']
```

### `str.strip([chars])`
Return a copy of the string with the leading and trailing characters removed. If omitted or `None`, the `chars` argument defaults to removing whitespace. *The `chars` argument is not a prefix or suffix; rather, all combinations of its values are stripped*:

```python
>>> '   spacious   '.strip()
'spacious'
>>> 'www.example.com'.strip('cmowz.')
'example'
```

The outermost leading and trailing `chars` argument values are stripped from the string. Characters are removed from the leading end until reaching a string character that is not contained in the set of characters in `chars`. A similar action takes place on the trailing end. For example:

```python
>>> comment_string = '#....... Section 3.2.1 Issue #32 .......'
>>> comment_string.strip('.#! ')
'Section 3.2.1 Issue #32'
```