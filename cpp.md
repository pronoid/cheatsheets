## Standard Template Library

### Vectors

#### Vector Initialize or allocate

copy elements from one to other with `operator=`, to move, use `a = std::move(b)`

```cpp
std::vector<int> v = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}; // or std::vector<int>v(10, 2);
// or std::vector<int> v {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
// or by copying other vector
// vector<int> vect2(vect1.begin(), vect1.end());
// std::vector<int> l(10); will keep all to zero
// std::iota(l.begin(), l.end(), -4); will result in -4 -3 -2 -1 0 1 2 3 4 5

// back_inserter(v) constructs back_insert_iterator
    std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::fill_n(std::back_inserter(v), 3, -1); 1 2 3 4 5 6 7 8 9 10 -1 -1 -1

        // use of fill_n
  std::vector<int> v1{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
 // note that next(v1.begin()) points to first elements, not v1.begin()
    std::fill_n(v1.begin(), 5, -1); -1 -1 -1 -1 -1 5 6 7 8 9

// or use fill
    std::vector<int> v{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

    std::fill(v.begin(), v.end(), -1);

// as of c++17, type can be omitted
 std::vector v {0, 1, 2};
```

#### Update

#### Extend with another

#### Iterate

```cpp
for(const auto&& x : values)
{
    // the contents of 'x' cannot be changed here...
    std::cout << x << std::endl;
}

// Getting a copy of the object
for(auto x : values)
{
    // the contents of 'x' can be changed here but we cannot change 'values'
    std::cout << x << std::endl;
}

// Getting a reference to the object to avoid copying
for(auto&& x : values)
{
    // here we can change the contents of 'x' and then also the contents of 'value'
    std::cout << x << std::endl;
}
```


### Initializer List

#### Initializer List Algorithms
`min`
```cpp
// possible implementation
template<class T, class Compare>
T min(std::initializer_list<T> ilist, Compare comp)
{
    return *std::min_element(ilist.begin(), ilist.end(), comp);
}

// In action

    std::cout << "smaller of 1 and 9999: " << std::min(1, 9999) << '\n'
              << "smaller of 'a', and 'b': " << std::min('a', 'b') << '\n'
              << "shortest of \"foo\", \"bar\", and \"hello\": " <<
                  std::min( { "foo", "bar", "hello" },
                                    // following is lambda
                            [](const std::string& s1, const std::string& s2) {
                                 return s1.size() < s2.size();
                             }) << '\n';
// outputs
smaller of 1 and 9999: 1
smaller of 'a', and 'b': a
shortest of "foo", "bar", and "hello": foo

```

[MinMax](https://en.cppreference.com/w/cpp/algorithm/minmax)
If there are only two elements, return reference to smaller and greater of two

If an initializer_list is given, returns the smallest(leftmost smallest in case of ties)
and the largest(rightmost largest) values.

```cpp
// bounds.first will always be less than or equal to bounds.second
    std::pair<int, int> bounds = std::minmax(std::rand() % v.size(),
                                             std::rand() % v.size());
```

### Sets

#### Initialize or allocate

#### Update

```cpp
// structured binding
       const auto [iter, inserted] = mySet.insert(value);

        if (inserted)
            std::cout << "Value(" << iter->n << ", " << iter->s << ", ...) was inserted" << "\n";
```

#### Extend with another

#### Membership: Check if element exists

```cpp
const bool is_in = container.find(element) != container.end();

// or using count
if (myset.count(x)) {
   // x is in the set, count is 1
} else {
   // count zero, i.e. x not in the set
}

// C++20 has contains function

if (myset.contains(x)) {
  // x is in the set
} else {
  // no x
}
```

#### Union, Intersection, Symmetric Difference, Includes

Note that std::set is not required. Following examples all take vectors as input


```cpp
std::vector<char> v1 {'a', 'b', 'c', 'f', 'h', 'x'};
std::vector<char> v6 {'A', 'B', 'C'};
// they can take comparators as optional parameter
 auto cmp_nocase = [](char a, char b) {
    return std::tolower(a) < std::tolower(b);
  };
bool includes = std::includes(v1.begin(), v1.end(), v6.begin(), v6.end(), cmp_nocase); // true
```

Difference

```cpp

std::vector<int> v1 {1, 2, 5, 5, 5, 9};
std::vector<int> v2 {2, 5, 7};
std::vector<int> diff;

std::set_difference(v1.begin(), v1.end(), v2.begin(), v2.end(),
                    std::inserter(diff, diff.begin()));
// 1 5 5 9
```

Symmetric difference

```cpp
std::vector<int> v1{1,2,3,4,5,6,7,8     };
std::vector<int> v2{        5,  7,  9,10};
std::sort(v1.begin(), v1.end());
std::sort(v2.begin(), v2.end());

std::vector<int> v_symDifference;

std::set_symmetric_difference(
    v1.begin(), v1.end(),
    v2.begin(), v2.end(),
    std::back_inserter(v_symDifference));
// 1 2 3 4 6 8 9 10
```

Union

```cpp
std::vector<int> v1 = {1, 2, 3, 4, 5, 5, 5};
std::vector<int> v2 = {      3, 4, 5, 6, 7};
std::vector<int> dest1;

std::set_union(v1.begin(), v1.end(),
               v2.begin(), v2.end(),
               std::back_inserter(dest1));
// 1 2 3 4 5 5 5 6 7
```

#### Iterate

### Maps

#### Initialize or allocate

```cpp
const std::map<std::string, std::string> capitals {
        { "South Africa", "Pretoria"},
        { "USA", "Washington"},
        { "France", "Paris"},
        { "UK", "London"},
        { "Germany", "Berlin"}
    };
```

#### Update

```cpp
std::map<char,int> mymap;
auto mapret = mymap.insert(std::pair('a', 100));
```

`insert` methods returns an `ok` value just like Go.

```cpp
auto [itelem, success] = mymap.insert(std::pair(’a’, 100));
If (!success) {
    // Insert failure
}

\\  or with Selection Initialization
if (auto [itelem, success] = mymap.insert(std::pair(‘a’, 100)); success) {
    // Insert success
}
```

Where itemelem is iterator to element, yes, a single element. It is deduced to be of the type

```cpp
std::map<char, int>::iterator itemelem;
```

### Iterating

```
for (const auto&[key, value] : mymap) {
    // Process entry using key and value
}
```


```cpp
capitals["South Africa"] = std::string("Johannesburg");
// erase by key
capitals.erase("UK");
// erase by iterator
// Remove a user by iterator
auto iterator = capitals.find("UK");
capitals.erase(iterator); // removes the entry ["UK", "London"]

// try emplace, c++17. Insert only if key doesn't exist
    std::map<std::string, std::string> m;

    m.try_emplace("a", "a"s); // s suffix is to instruct a compiler to make an
        /// std::string, otherwise it may construct a pointer to char
    m.try_emplace("b", "abcd");
    m.try_emplace("c", 10, 'c');
    m.try_emplace("c", "Won't be inserted");

            std::map<std::string, std::string> myMap;
    myMap.insert_or_assign("a", "apple"     );
    myMap.insert_or_assign("b", "bannana"   );
    myMap.insert_or_assign("c", "cherry"    );
    myMap.insert_or_assign("c", "clementine"); // c now has clementine
```

#### Membership: Checking if key exists

```cpp
if ( m.find("f") == m.end() ) {
  // not found
} else {
  // found
}

m.count(key) > 0
m.count(key) == 1
m.count(key) != 0

// c++17 using if statement with initializer

if ( auto it{ m.find( "key" ) }; it != std::end( m ) )
{
    // Use `structured binding` to get the key
    // and value.
    auto[ key, value ] { *it };

    // Grab either the key or value stored in the pair.
    // The key is stored in the 'first' variable and
    // the 'value' is stored in the second.
    auto mkey{ it->first };
    auto mvalue{ it->second };

    // That or just grab the entire pair pointed
    // to by the iterator.
    auto pair{ *it };
}
else
{
   // Key was not found..
}

// c++20 has contains
    if(example.contains(42)) {
        std::cout << "Found\n";
    } else {
        std::cout << "Not found\n";
    }
```

#### Extend with another

[Merge method](https://en.cppreference.com/w/cpp/container/map/merge)

Get all elements from other (move), emptying it.
If Key already exists, just point to its value, keeping the key-value intact
in the other.

```cpp
  std::map<int, std::string> ma {{1, "apple"}, {5, "pear"}, {10, "banana"}};
  std::map<int, std::string> mb {{2, "zorro"}, {4, "batman"}, {5, "X"}, {8, "alpaca"}};
  std::map<int, std::string> u;
  u.merge(ma);
  std::cout << "ma.size(): " << ma.size() << '\n';
  u.merge(mb);
  std::cout << "mb.size(): " << mb.size() << '\n';
  std::cout << "mb.at(5): " << mb.at(5) << '\n';
  for(auto const &kv: u)
    std::cout << kv.first << ", " << kv.second << '\n';

// prints
ma.size(): 0
mb.size(): 1
mb.at(5): X
1, apple
2, zorro
4, batman
5, pear
8, alpaca
10, banana
```

#### Iterate

```cpp
    // can you write code that shows prints Country: Capital ?
    // eg "Poland : Warsaw" ?
    for (auto && pair : capitals) {
        std::cout << pair.first << ": " << pair.second << std::endl;
}
```

## Algorithms

### Accumulate

Sum in $[first, last)$.

[Can do other reductions too with binary function](https://en.cppreference.com/w/cpp/algorithm/accumulate)

```cpp
    std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int sum = std::accumulate(v.begin(), v.end(), 0);

    int product = std::accumulate(v.begin(), v.end(), 1, std::multiplies<int>());
```

### Reduce

[Reduce behaves like std::accumulate except the elements of the range may be grouped and rearranged in arbitrary order](https://en.cppreference.com/w/cpp/algorithm/reduce)

This is much faster than accumulate when using parallel execution policy.

```cpp
double result = std::reduce(std::execution::par, v.begin(), v.end());
```

### Partial Sum

Loosely similar to Python `itertools.accumulate`. [ Optionally takes binary
](https://en.cppreference.com/w/cpp/algorithm/partial_sum) operator to do things other than sum.

```cpp
 std::vector<int> v = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}; // or std::vector<int>v(10, 2);

    std::cout << "The first 10 even numbers are: ";
        // prints The first 10 even numbers are: 2 4 6 8 10 12 14 16 18 20
    std::partial_sum(v.begin(), v.end(),
                     std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';

    std::partial_sum(v.begin(), v.end(), v.begin(), std::multiplies<int>());
        // prints The first 10 powers of 2 are: 2 4 8 16 32 64 128 256 512 1024
    std::cout << "The first 10 powers of 2 are: ";
    for (auto n : v) {
        std::cout << n << " ";
    }
    std::cout << '\n';
```

## Strings

### String find

[Find substrings, characters, optionally from a position](https://en.cppreference.com/w/cpp/string/basic_string/find)

### String substring

```cpp
std::string a = "0123456789abcdefghij";
std::string sub2 = a.substr(5, 3);
// prints 567
```

## Randomization

`std::mersenne_twister_engine` is implementation of by far [the most widely used](https://en.wikipedia.org/wiki/Mersenne_Twister)
pseudo-random number generator.

It has 32 and 64 bit unsigned int variants `std::mt19937` and `std::mt19937_64`.

```cpp
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(1, 6);
        for (int n=0; n<10; ++n)
        //Use dis to transform the random unsigned int generated by gen into an int in [1, 6]
        std::cout << dis(gen) << ' ';
        // outputs 1 1 6 5 2 2 5 5 6 2
```

### Random Shuffle `std::random_shuffle` and `std::shuffle`

```
    std::vector<int> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::random_device rd;
    std::mt19937 g(rd());

    std::shuffle(v.begin(), v.end(), g);
```

### Sample n elements

#### Without replacement

```cpp
    std::string in = "hgfedcba", out;
    std::sample(in.begin(), in.end(), std::back_inserter(out),
                5, std::mt19937{std::random_device{}()});
    std::cout << "five random letters out of " << in << " : " << out << '\n';
    // probably prints five random letters out of hgfedcba: gfcba
```

## Iterator

### Advance by n elements `std::advance`

```cpp
    std::vector<int> v{ 3, 1, 4 };
    auto vi = v.begin();
    std::advance(vi, 2);
    std::cout << *vi << '\n';
```

### Return nth successor (keep the original where it was)

`std::next` and `std::prev`

```cpp
    std::vector<int> v{ 3, 1, 4 };
    auto it = v.begin();
    auto nx = std::next(it, 2);
    std::cout << *it << ' ' << *nx << '\n';
    // prints 3 4 (it points to 3 and nx to 4)

    std::vector<int> v{ 3, 1, 4 };
    auto it = v.end();
    auto pv = std::prev(it, 2);
    std::cout << *pv << '\n'; // prints 1
```


## Sorting

`std::sort`

```cpp
std::array<int, 10> s = {5, 7, 4, 2, 8, 6, 1, 9, 0, 3};
// using default operator<
std::sort(s.begin(), s.end()); // modifies underlying container

// sort in reverse using a standard library compare function object
std::sort(s.begin(), s.end(), std::greater<int>());

    // sort using a custom function object
struct {
    bool operator()(int a, int b) const
    {
        return a < b;
    }
} customLess;
std::sort(s.begin(), s.end(), customLess);

    // sort in reverse using a lambda expression
std::sort(s.begin(), s.end(), [](int a, int b) {
    return a > b;
});
```

### selection

`std::nth_element`, takes comparator too.

```cpp
std::vector<int> v{5, 6, 4, 3, 2, 6, 7, 9, 3};

std::nth_element(v.begin(), v.begin() + v.size()/2, v.end());
std::cout << "The median is " << v[v.size()/2] << '\n';

std::nth_element(v.begin(), v.begin()+1, v.end(), std::greater<int>());
std::cout << "The second largest element is " << v[1] << '\n';

// prints
// The median is 5
// The second largest element is 7
```

### Partitioning

`std::partition` [reorders the elements](https://en.cppreference.com/w/cpp/algorithm/partition) in such a way that all elements for which the predicate p returns true precede the elements for which predicate p returns false.

*Note that this returns an iterator to second group and thre predicate it takes is unary where as the predicate that lower_bound takes is binary.*

`std::partition_point` further helps find the parition point (it does not partition though, assumes that range is already partitioned)
[See how to implement lower_bound with partition](https://stackoverflow.com/questions/51050104/what-is-the-difference-between-partition-point-and-lower-bound)

`std::stable_partition` variant preserves the relative order of elements.

#### Possible implementation

```cpp
template<class ForwardIt, class UnaryPredicate>
ForwardIt partition(ForwardIt first, ForwardIt last, UnaryPredicate p)
{
    first = std::find_if_not(first, last, p);
    if (first == last) return first;

    for (ForwardIt i = std::next(first); i != last; ++i) {
        if (p(*i)) {
            std::iter_swap(i, first);
            ++first;
        }
    }
    return first;
}
```

#### Example

```cpp
template <class ForwardIt>
 void quicksort(ForwardIt first, ForwardIt last)
 {
    if(first == last) return;
    auto pivot = *std::next(first, std::distance(first,last)/2);
    ForwardIt middle1 = std::partition(first, last,
                         [pivot](const auto& em){ return em < pivot; });
    ForwardIt middle2 = std::partition(middle1, last,
                         [pivot](const auto& em){ return !(pivot < em); });
    quicksort(first, middle1);
    quicksort(middle2, last);
 }

std::vector<int> v = {0,1,2,3,4,5,6,7,8,9};
std::cout << "Original vector:\n    ";
for(int elem : v) std::cout << elem << ' ';

auto it = std::partition(v.begin(), v.end(), [](int i){return i % 2 == 0;});

std::cout << "\nPartitioned vector:\n    ";
std::copy(std::begin(v), it, std::ostream_iterator<int>(std::cout, " "));
std::cout << " * ";
std::copy(it, std::end(v), std::ostream_iterator<int>(std::cout, " "));

std::forward_list<int> fl = {1, 30, -4, 3, 5, -4, 1, 6, -8, 2, -5, 64, 1, 92};
std::cout << "\nUnsorted list:\n    ";
for(int n : fl) std::cout << n << ' ';
std::cout << '\n';

quicksort(std::begin(fl), std::end(fl));
std::cout << "Sorted using quicksort:\n    ";
for(int fi : fl) std::cout << fi << ' ';
std::cout << '\n';

// Output:
//
// Original vector:
//     0 1 2 3 4 5 6 7 8 9
// Partitioned vector:
//     0 8 2 6 4  *  5 3 7 1 9
// Unsorted list:
//     1 30 -4 3 5 -4 1 6 -8 2 -5 64 1 92
// Sorted using quicksort:
//     -8 -5 -4 -4 1 1 1 2 3 5 6 30 64 92
```

#### Without modifying in place

[Use partition_copy](https://en.cppreference.com/w/cpp/algorithm/partition_copy)

```cpp
int arr [10] = {1,2,3,4,5,6,7,8,9,10};
int true_arr [5] = {0};
int false_arr [5] = {0};

std::partition_copy(std::begin(arr), std::end(arr), std::begin(true_arr),std::begin(false_arr), [] (int i) {return i > 5;});
```

### Binary Search

`std::lower_bound` returns an iterator pointing to the first element in the range $[first, last)$ that is not less than (i.e. greater or equal to) value, or *last if no such element is found*. It's cousin is `std::upper_bound`.

These two are not strict counterparts of python `bisect_left` and `bisect_right`. bisect module returns insertion points even if element is not present.

```cpp
 std::vector<int> data = { 1, 1, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 6 };

auto lower = std::lower_bound(data.begin(), data.end(), 4);
auto upper = std::upper_bound(data.begin(), data.end(), 4);

std::copy(lower, upper, std::ostream_iterator<int>(std::cout, " "));

std::cout << '\n';

// classic binary search, returning a value only if it is present

data = { 1, 2, 4, 6, 9, 10 };

auto it = binary_find(data.cbegin(), data.cend(), 4); // NOTE: choosing '5' will return end()

if(it != data.cend())
  std::cout << *it << " found at index "<< std::distance(data.cbegin(), it);

return 0;

// 4 4 4
// 4 found at index 2
```

#### Range of equal elements in sorted array `std::equal_range`

Returns `std::pair` start and end (elements from $[start, end-1]$ are equal)

### Linear Search (useful for strings)

Searches for the first occurrence of the sequence of elements [s_first, s_last) in the range [first, last). Takes [optional searcher](https://en.cppreference.com/w/cpp/utility/functional/boyer_moore_horspool_searcher)

```cpp
// The C++17 overload demo:
 std::string in = "Lorem ipsum dolor sit amet, consectetur adipiscing elit,"
                  " sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
 std::string needle = "pisci";
 auto it = std::search(in.begin(), in.end(),
                std::boyer_moore_searcher( // searcher is optional
                   needle.begin(), needle.end()));
```

### Functors or Function Objects

A lot of algorithms in STL take Predicates (unary or binary) as optional argument. See [`std::all_of`](https://en.cppreference.com/w/cpp/algorithm/all_any_none_of) for example. In C++ parlance, a [functor is an object with `operator()` defined](https://www.cs.helsinki.fi/u/tpkarkka/alglib/k06/lectures/functors.html)

We could define a functor

```cpp
struct DivisibleBy
    {
        const int d;
        DivisibleBy(int n) : d(n) {} // a constructor, which assigns n to field d.
        bool operator()(int n) const { return n % d == 0; }
    };
```

and use it thusly,

```cpp
    std::vector<int> v(10, 2);
    std::partial_sum(v.cbegin(), v.cend(), v.begin()); // this can take optional functor as well. Default is sum
    std::cout << "Among the numbers: ";
    std::copy(v.cbegin(), v.cend(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';
    if (std::any_of(v.cbegin(), v.cend(), DivisibleBy(7))) {
        std::cout << "At least one number is divisible by 7\n";
    }
// prints
// Among the numbers: 2 4 6 8 10 12 14 16 18 20
// At least one number is divisible by 7
```

functors are much faster than function pointers as they are probably inlined. And the type identifies the functionality.

```cpp
struct less {
  bool operator() (int a, int b) const { return a < b; }
};
struct greater {
  bool operator() (int a, int b) const { return a > b; }
};

// these two sets have a different type
set<int, less> s1;
set<int, greater> s2;

bool is_less (int a, int b) { return a < b; }
bool is_greater (int a, int b) { return a > b; }

// but these two have the same type
set<int, bool (*)(int,int)> s1(is_less);
set<int, bool (*)(int,int)> s2(is_greater);
```

STL has [predefined functors](https://www.bogotobogo.com/cplusplus/functors.php)

```cpp
set<int> mySet;
```

is internally

```cpp
set<int, less<int> > mySet;
```

If we want to store elements in decreasing order:

```cpp
set<int, greater<int> > mySet;
```

## Other helpful utilities

### Initializing sentinels with <limits>

Use`std::numeric_limits<T>::lowest()`, `std::numeric_limits<T>::min()`, and `std::numeric_limits<T>::max()`.

*Note that `std::numeric_limits<double>::min()` provides minimum positive value of double. Whereas std::numeric_limits<double>::lowest() will provide lowest (negative) value.*

### `std::midpoint` of `<numeric>` to find midpoint avoiding overflow

```cpp
std::uint32_t a = std::numeric_limits<std::uint32_t>::max();
std::uint32_t b = std::numeric_limits<std::uint32_t>::max() - 2;

std::cout << "a: " << a << '\n'
              << "b: " << b << '\n'
              << "Incorrect (overflow and wrapping): " << (a + b) / 2 << '\n'
              << "Correct: " << std::midpoint(a, b) << '\n';

// prints
// a: 4294967295
// b: 4294967293
// Incorrect (overflow and wrapping): 2147483646
// Correct: 4294967294
```

## Returning Multiple Values

```cpp
auto mytuple()
{
    char a = 'a';
    int i = 123;
    bool b = true;
    return std::tuple(a, i, b);  // No types needed
}
```

or even simpler

```cpp
auto mytuple()
{
    return std::tuple(‘a’, 123, true);  // Auto type deduction from arguments
}
```

Using ** in C++17, we can just use *Structured Binding*, which works with `pair`, `tuple`, and `struct`

```cpp
auto [a, i, b] = mytuple();
```

## Selection initialization

Just like Go

```cpp
if (auto [a, b] = myfunc(); a < b) {
    // Process using a and b
}
```
