# Data structures

## Dict
`defaultdict` and `Counter` inherit from it.

```python
from collections import Counter
c = Counter(cats=4, dogs=8)

>>> c.values()
dict_values([4,3])

>>> for animal in c:
...     print(animal)
...
cats
dogs

>>> knights = {'gallahad': 'the pure', 'robin': 'the brave'}
# Note items method below
>>> for k, v in knights.items():
...     print(k, v)
...
gallahad the pure
robin the brave
```

## Heap/priority queue
from heapq import heappush, heappop

```python
tasks = ["A","A","A","B","B","B"]
n = 2
task_freqs = Counter(tasks)
task_heap = []
for task in task_freqs:
# Note: Doesn't take a key,
# just (priority, task) tuple
    heappush(task_heap, (task_freqs[task], task)) 
    if not task_heap:
        continue
    popped = heappop(task_heap)
```

## deque
`deque([iterable[, maxlen]])`

If `maxlen` is not specified or is `None`, deques may grow to an arbitrary length.

Otherwise, the deque is bounded to the specified maximum length. Once a bounded length deque is full, when new items are added, a corresponding number of items are discarded from the opposite end.

```python

append(x)
    # Add x to the right side of the deque.

appendleft(x)
   # Add x to the left side of the deque.

pop()
   # Remove and return an element from the right side of the deque. If no elements are present, raises an IndexError.

popleft()
   # Remove and return an element from the left side of the deque. If no elements are present, raises an IndexError.
```

## defaultdict
`collections.defaultdict([default_factory[, ...]])`

The first argument provides the initial value for the `default_factory` attribute; it defaults to `None`.

## Min-heap
`from heapq import heappush, heappop`

Using a heap to insert items at the correct place in a priority queue:

```python
>>> heap = []
>>> data = [(1, 'J'), (4, 'N'), (3, 'H'), (2, 'O')]
>>> for item in data:
...     heappush(heap, item) # first element used as priority
...
>>> while heap:
...     print(heappop(heap)[1])
J
O
H
N
```

## Namedtuple
```python
>>> # Basic example
>>> Point = namedtuple('Point', ['x', 'y'])
>>> p = Point(11, y=22)     # instantiate with positional or keyword arguments
>>> p[0] + p[1]             # indexable like the plain tuple (11, 22)
33
>>> x, y = p                # unpack like a regular tuple
>>> x, y
(11, 22)
>>> p.x + p.y               # fields also accessible by name
33
>>> p                       # readable __repr__ with a name=value style
Point(x=11, y=22)
```

Named tuples are especially useful for assigning field names to result tuples returned by the csv or sqlite3 modules:
```python
EmployeeRecord = namedtuple('EmployeeRecord', 'name, age, title, department, paygrade')
```

# Bisect
`bisect.bisect_left(list, item[, lo[, hi]])`
Locate the proper insertion point for item in list to maintain sorted order. If `item` is already present in `list`, the insertion point will be before (to the left of) any existing entries.

`bisect.bisect_right(list, item[, lo[, hi]])` and `bisect.bisect(...)`
Similar to `bisect_left()`, but returns an insertion point which comes after (to the right of) any existing entries of item in list.